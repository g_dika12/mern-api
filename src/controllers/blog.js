const { validationResult } = require('express-validator')
const BlogPost = require('../model/blog'); // untuk simpan ke database
const path = require('path')
const fs = require('fs');

exports.createBlogPost = (req, res, next) => {
    const title = req.body.title;
    const image = req.file.path; // <==> multer
    const body = req.body.body;

    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        const err = new Error("Input value tidak sesuai");
        err.errorStatus = 400;
        err.data = errors.array();
        throw err;
    }

    if (!req.file) { // berhubungan dengan multer -> simpan image
        const err = new Error("Image harus di upload...")
        err.errorStatus = 422;
        throw err;
    }

    const Posting = new BlogPost({
        title: title,
        body: body,
        image: image,
        author: {
            uid: 1,
            name: "Dika",

        }
    });
    Posting.save() // simapn di database
        .then(result => {
            res.status(201).json({
                message: "Create Blog Post Success",
                data: result
            })
        })
        .catch(err => {
            console.log('POSTING SAVE err ', err)
        })
}

exports.getAllBlogPost = (req, res, next) => {
    const currentPage = req.query.page || 1; // default = 1
    const perPage = req.query.perPage || 5; // default = 5
    let totalItems;

    // Panggil dari DB
    BlogPost.find()
        .countDocuments()
        .then(count => {
            totalItems = count;
            return BlogPost.find()
                .skip((parseInt(currentPage) - 1) * parseInt(perPage))
                .limit(parseInt(perPage));
        })
        .then(result => {
            res.status(200).json({
                message: "Data Blog Post berhasil dipanggil",
                data: result,
                total_data: totalItems,
                per_page: parseInt(perPage),
                current_page: parseInt(currentPage)
            })
        })
        .catch(err => next(err))

}

exports.getBlogPostById = (req, res, next) => {
    const postId = req.params.postID;
    BlogPost.findById(postId)
        .then(result => {
            if (!result) {
                const error = new Error('Blog Post tidak ditemukan')
                error.errorStatus = 404;
                throw error
            }
            res.status(200).json({
                message: "Data Blog Post by ID berhasil dipanggil",
                data: result
            })
        })
        .catch(err => { next(err) })
}

exports.updateBlogPost = (req, res, next) => {
    const title = req.body.title; // data baru input
    const image = req.file.path;
    const body = req.body.body;
    const postId = req.params.postID;

    const errors = validationResult(req)

    if (!errors.isEmpty()) { // jika error tidak kosong
        const err = new Error("Input value tidak sesuai")
        err.errorStatus = 400;
        err.data = errors.array();
        throw err;
    }

    if (!req.file) {
        const err = new Error("Image harus di upload...")
        err.errorStatus = 422;
        throw err;
    }



    BlogPost.findById(postId)
        .then(post => {
            if (!post) {
                const err = new Error('Blog post tidak ditemukan!')
                error.errorStatus = 404;
                throw err;
            }
            post.title = title;
            post.body = body;
            post.image = image;

            return post.save() // fungsi akan return Promise '.then()' bila berhasil
        })
        .then(result => {
            res.status(200).json({
                message: "Update data berhasil..",
                data: result
            })
        })
        .catch(err => next(err))
}

exports.deleteBlogPost = (req, res, next) => {
    const postId = req.params.postID;
    BlogPost.findById(postId)
        .then(post => {

            if (!post) {
                const err = new Error('Blog post tidak ditemukan!')
                error.errorStatus = 404;
                throw err;
            }

            // hapus foto
            removeImage(post.image) // data get dari DB
            return BlogPost.findByIdAndRemove(postId)
        })
        .then(result => {
            res.status(200).json({
                message: "Delete Blog berhasil...",
                data: result
            })

        })
        .catch(err => next(err))

}
const removeImage = (filePath) => {
    // img path = images/1624720226762 check.png
    // console.log('+++ ', __dirname)
    // +++ /Users/guitherezsinaga/Documents/iCloud Drive-(Archive)/Documents/DEVELOPMENT/NODE/mern-api/src/controllers
    // console.log(path.join(__dirname, "../../")) // pindah 2 direktori dari ('src/controllers')
    // /Users/guitherezsinaga/Documents/iCloud Drive-(Archive)/Documents/DEVELOPMENT/NODE/mern-api/
    // imagePath = path.join(__dirname + "../../" + imagePath);
    filePath = path.join(__dirname, "../../", filePath);
    // /Users/guitherezsinaga/Documents/iCloud Drive-(Archive)/Documents/DEVELOPMENT/NODE/mern-api/src/images/1624720226762 check.png

    // hapus
    fs.unlink(filePath, err => console.log(err))
}