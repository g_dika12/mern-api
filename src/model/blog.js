const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BlogPost = new Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    image:{
        type: String, 
        required: true 
        // mongoDB hanya simpan url image/path
        // file image disimpan dalam server project 
        // pengisian data pada POSTMAN --> Body 'form-data' bukan Body 'raw'

    },
    author: {
        type: Object,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('BlogPost', BlogPost);

// postman -- > BODY Form-Data
/*
KEY                             DESCRIPTION
title                           Dika Blog
body                            Dika Blog kjhkjhkHKH kjhkjhkjh jhkhjk
image (pilih "file")            Select File // button => upload file dari komputer
*/

// CEK IMAGE BY POSTMAN// http://localhost:4000/images/1624720226762 check.png
