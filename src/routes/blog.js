const express = require('express');
const router = express.Router();
const { body } = require('express-validator');

const blogController = require('../controllers/blog')
// [POST]: /v1/blog/post
// router.post('/post', blogController.createBlogPost)
router.post('/post', [
    body('title').isLength({ min: 5 }).withMessage("input title is invalid"),
    body('body').isLength({ min: 5 }).withMessage("input body is invalid")
],
    blogController.createBlogPost
)

router.get('/posts', blogController.getAllBlogPost) // semua data database
// page=1 = start dari page 1; perPage = 5 => 5 data/page;
// localhost:4000/v1/blog/posts?page=2
// localhost:4000/v1/blog/posts?page=2&perPage=10;

router.get('/post/:postID', blogController.getBlogPostById);
router.put('/post/:postID',[ // validator
    body('title').isLength({ min: 5}).withMessage("input title is invalid"),
    body('body').isLength({ min: 5}).withMessage("input body is invalid"),
], blogController.updateBlogPost)
// pemanggilan di POSTMAN -> body -> form-data ;; untuk update image 
router.delete('/post/:postID', blogController.deleteBlogPost);

module.exports = router;