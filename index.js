const express = require('express');
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
const multer = require('multer');
const path = require('path')

const app = express();
const authRoutes = require('./src/routes/auth')
const blogRoutes = require('./src/routes/blog')

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images') // buat folder baru 'images' di root project
        // null = tidak error; 'images' asset folder pada root project (storage) 
    },
    filename: (req, file, cb) => {
        cb(null, new Date().getTime() + " " + file.originalname) // file.originalName = default multer
    }
})

const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ) {
        cb(null, true) // true = file diterima
    } else {
        cb(null, false)
    }
}

app.use(bodyParser.json()) // catch data JSON dari client // middleware
app.use('/images', express.static(path.join(__dirname, 'images')))
app.use(multer({storage: fileStorage, fileFilter: fileFilter}).single('image')) // "image" => request body pada POSTMAN 

//AKSES WEB KE API
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*"); // "*" ==  semua web bisa ngakses
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization")
    next();
})

app.use('/v1/auth', authRoutes);
app.use('/v1/blog', blogRoutes);
app.use((error, req, res, next) => {
    const status = error.errorStatus || 500;
    const message = error.message;
    const data = error.data;

    // res.status(400).json({message:'Error', data:"data disini!!"})
    res.status(status).json({ message: message, data: data })
})
const config = {
    autoIndex: false,
    useNewUrlParser: true,
}

const url = 'mongodb+srv://mahardika:9uv7DmlDvMXhfjdo@cluster0.bu23a.mongodb.net/blogDB?retryWrites=true&w=majority';
mongoose.connect(url,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        app.listen(4000, () => { console.log('?--->  connection to mongoDB succeed') });
    })
    .catch(err => console.log('ERR INDEX MONGOOSE = ', err))

    